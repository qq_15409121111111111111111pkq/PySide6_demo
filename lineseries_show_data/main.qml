// main.qml
import QtQuick 2.15
import QtQuick.Controls 2.15
import QtCharts 2.3

Window {
    visible: true
    width: 800
    height: 600
    title: "Data Generator Example"

    ChartView {
        id: chart
        anchors.fill: parent
        antialiasing: true

        ValueAxis {
            id: axisX
            min: 0
            max: 10
        }

        ValueAxis {
            id: axisY
            min: 0
            max: 100
        }

        LineSeries {
            id: lineSeries
            axisX: axisX
            axisY: axisY
        }
    }

    Connections {
        target: backend
        function onDataGenerated(elapsedTime,value) {
            lineSeries.append(elapsedTime, value);
            axisX.max = elapsedTime + 1;
            
        }
    }
}