# main.py
from PySide6.QtCore import QObject, Slot, Signal, QTimer, QUrl
from PySide6.QtCharts import QChart, QLineSeries
from PySide6.QtWidgets import QApplication
from PySide6.QtQml import QQmlApplicationEngine
from PySide6.QtGui import QGuiApplication
import sys
import random
import time
class DataGenerator(QObject):
    dataGenerated = Signal(float,float)  # 定义一个信号，用于发送新产生的数据

    def __init__(self):
        super().__init__()
        self._timer = QTimer(self)
        self._timer.timeout.connect(self._generate_data)
        self._timer.setInterval(500)  # 每秒生成一次新数据
        self._start_time = time.time()
    @Slot()
    def start(self):
        """开始生成数据"""
        self._start_time = time.time()
        self._timer.start()

    @Slot()
    def stop(self):
        """停止生成数据"""
        self._timer.stop()

    def _generate_data(self):
        """生成随机数据并发送信号"""
        elapsed_time = time.time() - self._start_time
        value = random.uniform(0, 100)  # 模拟从外部设备获取数据的过程
        self.dataGenerated.emit(elapsed_time,value)  # 发送新值给QML界面

if __name__ == "__main__":
    app = QApplication(sys.argv)
    engine = QQmlApplicationEngine()

    generator = DataGenerator()
    engine.rootContext().setContextProperty("backend", generator)

    engine.load("main.qml")
    if not engine.rootObjects():
        sys.exit(-1)

    generator.start()  # 启动数据生成器
    sys.exit(app.exec())