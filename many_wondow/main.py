from PySide6.QtGui import QGuiApplication
from PySide6.QtQml import QQmlApplicationEngine,QQmlComponent
from PySide6.QtCore import QObject,Slot
import sys

class MainWindow(QObject):
    def __init__(self):
        super().__init__()
        self.engine = QQmlApplicationEngine()
        self.engine.rootContext().setContextProperty("main_window",self)
        self.engine.load("test_window/main_window.qml")
        if not self.engine.rootObjects():
            sys.exit(-1)
        self.main_window = self.engine.rootObjects()[0]

        self.first_window_component = QQmlComponent(self.engine)
        self.first_window_component.loadUrl("test_window/first_window.qml")
        if self.first_window_component.isReady():
            self.first_window = self.first_window_component.create()
        else:
            print(self.first_window_component.errors())

        self.second_window_component = QQmlComponent(self.engine)
        self.second_window_component.loadUrl("test_window/second_window.qml")
        if self.second_window_component.isReady():
            self.second_window = self.second_window_component.create()
        else:
            print(self.second_window_component.errors())

    @Slot()
    def show_first_window(self):
        self.first_window.setVisible(True)

    @Slot()
    def show_second_window(self):
        self.second_window.setVisible(True)

    @Slot()
    def close_mainwindow(self):
        self.main_window.close()

    @Slot()
    def close_first_window(self):
        self.first_window.setVisible(False)






if __name__ == "__main__":
    app = QGuiApplication(sys.argv)
    main_window = MainWindow()

    sys.exit(app.exec())
