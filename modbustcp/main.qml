import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15
import QtCharts 2.15

Window {
    visible: true
    width: 1240
    height: 900
    title: "Modbus TCP Client"
    color:"gray"

    property bool connected: false
    Column{
        anchors.fill: parent
        anchors.topMargin:50
        spacing:20
        Column{
            anchors.fill: parent
            spacing: 20
            Button {
                id: connectButton
                x:50;y:50
                text: connected ? "已连接" : "未连接"
                width:200;height:50
                onClicked: {
                    if (!connected) {
                        pythonBridge.connect_to_modbus()
                        connected = true
                    } else {
                        pythonBridge.disconnect_from_modbus()
                        connected = false
                        lineSeries1.clear();
                        lineSeries2.clear();
                        lineSeries3.clear();
                        lineSeries4.clear();
                    }
                }
            }

            Button{
                id:collectButton
                x:50;y:50
                width:200;height:50
                text:qsTr("开始采集")
                onClicked:{
                    if (text === "开始采集"){
                        pythonBridge.start_collect()
                        text = "停止采集"
                    } else {
                        pythonBridge.stop_collect()
                        text = "开始采集"
                    }
                }
            }

            Button{
                id:savetocsv
                x:50;y:50
                width:200;height:50
                text:qsTr("保存数据")
                onClicked:{
                    var filename = pythonBridge.get_save_filename()
                    pythonBridge.save_to_csv(filename)
                }
            }
        }


        Column {
            anchors.fill: parent
            anchors.leftMargin: 600
            anchors.topMargin: 50
            spacing: 10

            Row {
                spacing: 30

                Text {
                    text: "xTcp:"
                    font.bold: true
                    font.pointSize: 14
                    width: 50
                }

                Rectangle {
                    width: 100
                    height: 30

                    border.color: "black"
                    border.width: 1
                    Text {
                        objectName:"xTcp"
                        anchors.centerIn: parent
                        text: "0" // 随机浮点数
                    }
                }

                Text {
                    text: "x力矩:"
                    font.bold: true
                    font.pointSize: 14
                    width: 50
                }

                Rectangle {
                    width: 100
                    height: 30
                    border.color: "black"
                    border.width: 1
                    Text {
                        objectName:"x力矩"
                        anchors.centerIn: parent
                        text: "0" // 随机浮点数
                    }
                }
            }

            Row {
                spacing: 30

                Text {
                    text: "yTcp:"
                    font.bold: true
                    font.pointSize: 14
                    width: 50
                }

                Rectangle {
                    width: 100
                    height: 30

                    border.color: "black"
                    border.width: 1
                    Text {
                        objectName:"yTcp"
                        anchors.centerIn: parent
                        text: "0" // 随机浮点数
                    }
                }

                Text {
                    text: "y力矩:"
                    font.bold: true
                    font.pointSize: 14
                    width: 50
                }

                Rectangle {
                    width: 100
                    height: 30
                    border.color: "black"
                    border.width: 1
                    Text {
                        objectName:"y力矩"
                        anchors.centerIn: parent
                        text: "0" // 随机浮点数
                    }
                }
            }

            Row {
                spacing: 30

                Text {
                    text: "zTcp:"
                    font.bold: true
                    font.pointSize: 14
                    width: 50
                }

                Rectangle {
                    width: 100
                    height: 30

                    border.color: "black"
                    border.width: 1
                    Text {
                        objectName:"zTcp"
                        anchors.centerIn: parent
                        text: "0" // 随机浮点数
                    }
                }

                Text {
                    text: "z力矩:"
                    font.bold: true
                    font.pointSize: 14
                    width: 50
                }

                Rectangle {
                    width: 100
                    height: 30
                    border.color: "black"
                    border.width: 1
                    Text {
                        objectName:"z力矩"
                        anchors.centerIn: parent
                        text: "0"// 随机浮点数
                    }
                }
            }
        }

        Rectangle{
            anchors.bottom:parent.bottom
            objectName:"chart"
            width:1240;height:600
            color:"lightgrey"

            Row{
                anchors.fill: parent
                anchors.topMargin:50
                anchors.leftMargin:10
                spacing: 10

                ChartView{
                    id:pz
                    width:400;height:500
                    title:"ZTcp vs. Time"
                    legend.visible: false
                    antialiasing: true

                    ValueAxis{
                        id:axisX1
                        min:0
                        max:10
                        labelFormat: "%.2f"
                        titleText:"Time"
                    }

                    ValueAxis{
                        id:axisY1
                        min:-100
                        max:100
                        labelFormat:"%.2f"
                        titleText:"ZTcp"
                    }

                    LineSeries{
                        id:lineSeries1
                        name:"ZTcp"
                        axisX:axisX1
                        axisY:axisY1
                    }
                    
                }

                ChartView{
                    objectName:"Fz"
                    id:fz
                    width:400;height:500
                    title:"Fz vs Time"
                    antialiasing:true
                    legend.visible:false

                    ValueAxis{
                        id:axisX2
                        min:0
                        max:100
                        labelFormat:"%.2f"
                        titleText:"Fz"
                    }

                    ValueAxis{
                        id:axisY2
                        min:-5
                        max:5
                        labelFormat:"%.2f"
                        titleText:"Time"
                    }

                    LineSeries{
                        id:lineSeries2
                        name:"Fz"
                        axisX:axisX2
                        axisY:axisY2
                    }
                }

                ChartView{
                    id:fzvsft
                    width:400;height:500
                    title:"FzandFt vs Time"
                    antialiasing:true
                    legend.visible:false

                    ValueAxis{
                        id:axisX3
                        min:0
                        max:100
                        labelFormat:"%.2f"
                        titleText:"Fz"
                    }

                    ValueAxis{
                        id:axisY3
                        min:0
                        max:100
                        labelFormat:"%.2f"
                        titleText:"Time"
                    }

                    LineSeries{
                        id:lineSeries3
                        name:"Fz"
                        axisX:axisX3
                        axisY:axisY3
                    }

                    LineSeries{
                        id:lineSeries4
                        name:"trueFz"
                        axisX:axisX3
                        axisY:axisY3
                        color: "red"
                    }
                }
                Connections {
                    target: pythonBridge
                    function onLinedata(elapsedTime,pz,fz) {
                        lineSeries1.append(elapsedTime, pz);
                        lineSeries2.append(elapsedTime, fz);
                        axisY1.max = pz * 2;
                        axisY1.min = -1 * (pz * 2);
                        axisY2.max = fz * 2;
                        axisY2.min = -1 * (fz * 2);
                        axisX1.max = elapsedTime + 5;
                        axisX1.min = elapsedTime - 30;
                        axisX2.max = elapsedTime + 5;
                        axisX2.min = elapsedTime - 30;

                    }

                    function onCompare(elapsedTime,fz,a){
                        lineSeries3.append(elapsedTime, fz);
                        lineSeries4.append(elapsedTime, a);
                        axisY3.max = fz * 2;
                        axisY3.min = -1 * (fz * 2);
                        axisX3.max = elapsedTime + 5;
                        axisX3.min = elapsedTime - 30;
                    }
                }
            }
        }
    }
}