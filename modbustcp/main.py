import sys
from PySide6.QtWidgets import QApplication,QFileDialog
from PySide6.QtGui import QGuiApplication
from PySide6.QtQml import QQmlApplicationEngine, QQmlContext
from PySide6.QtCore import QObject,Slot,QTimer,QThread,Signal
from pymodbus.client.sync import ModbusTcpClient
from PySide6.QtCharts import QXYSeries,QLineSeries,QValueAxis
import struct
import time
import csv
import random
class ModbusClient(QObject):
    linedata = Signal(float,float,float)
    compare = Signal(float,float,float)
    def __init__(self,root_object,parent=None):
        super().__init__()
        self.root_object = root_object
        self.client = ModbusTcpClient('192.168.2.100', port=502)
        self.connected = False
        self.timer = QTimer(self)
        self.timer.timeout.connect(self.read_data)
        self.coordinates = []   
        self._start_time = time.time()
        self.compare_ = False
        

    @Slot()
    def connect_to_modbus(self):
        self._start_time = time.time()
        if not self.connected:
            self.client.connect()
            if self.client.connect():
                self.connected = True
                print("Connected to Modbus server")
                self.timer.start(500)
                #self.read_data()
            else:
                print("Failed to connect to Modbus server")

    @Slot()
    def disconnect_from_modbus(self):
        if self.connected:
            self.client.close()
            self.connected = False
            print("Disconnected from Modbus server")
            self.timer.stop()

    @Slot()
    def start_collect(self):
        self.compare_ = True

    @Slot()
    def stop_collect(self):
        self.compare_ = False

    def read_data(self):
        if self.connected:
            elapsed_time = time.time() - self._start_time
            a = random.uniform(1, 10)
            result = self.client.read_holding_registers(address=0x54, count=6, unit=1)
            result_1 = self.client.read_holding_registers(address=0x54 + 24, count=6, unit=1)
            ones_coordinates = []
            if result.isError():
                print("Failed to read data")
            else:
                data = result.registers
                data_1 = result_1.registers
                

                for i in range(0,6,2):
                    combined_value = (data[i + 1] << 16) | data[i]
                    coordinates_value = struct.unpack('!f',struct.pack('!I',combined_value))[0]
                    ones_coordinates.append(coordinates_value)
                    if self.compare_:
                        self.coordinates.append(coordinates_value)

                for i in range(0,6,2):
                    combined_value_1 = (data_1[i + 1] << 16) | data_1[i]
                    coordinates_value_1 = struct.unpack('!f',struct.pack('!I',combined_value_1))[0]
                    ones_coordinates.append(coordinates_value_1)
                    if self.compare_:
                        self.coordinates.append(coordinates_value_1)
                pz = ones_coordinates[2] * 1000
                fz = ones_coordinates[5]
                self.linedata.emit(elapsed_time,pz,fz)
                self.update_labels(ones_coordinates)
                if self.compare_:
                    self.compare.emit(elapsed_time,fz,a)

    @Slot()
    def update_labels(self, data):
        self.root_object.findChild(QObject,"xTcp").setProperty("text", f"{(data[0] * 1000):.3f}")
        self.root_object.findChild(QObject,"x力矩").setProperty("text", f"{(data[3]):.3f}")
        self.root_object.findChild(QObject,"yTcp").setProperty("text", f"{(data[1] * 1000):.3f}")
        self.root_object.findChild(QObject, "y力矩").setProperty("text", f"{(data[4]):.3f}")
        self.root_object.findChild(QObject, "zTcp").setProperty("text", f"{(data[2] * 1000):.3f}")
        self.root_object.findChild(QObject, "z力矩").setProperty("text", f"{(data[5]):.3f}")

    @Slot(result = str)
    def get_save_filename(self):
        options = QFileDialog.Options()
        filename,_ = QFileDialog.getSaveFileName(None,"QFileDialog.getSaveFileName()","","CSV Files (*.csv);;All Files (*)",options = options)
        return filename
    @Slot(str)
    def save_to_csv(self, filename):
        with open(filename, mode='w', newline='') as file:
            writer = csv.writer(file)
            # 写入表头
            writer.writerow(['Px', 'Py', 'Pz', 'Fx', 'Fy', 'Fz'])
            
            # 每六个数据为一组
            for i in range(0, len(self.coordinates), 6):
                row = self.coordinates[i:i+6]
                if len(row) == 6:  # 确保有六个数据
                    writer.writerow(row)

if __name__ == "__main__":
    app = QApplication(sys.argv)
    engine = QQmlApplicationEngine()
    engine.load("main.qml")
    root_object = engine.rootObjects()[0]
    modbus_client = ModbusClient(root_object)
    engine.rootContext().setContextProperty("pythonBridge", modbus_client)
    
    if not engine.rootObjects():
        sys.exit(-1)
  

    sys.exit(app.exec())